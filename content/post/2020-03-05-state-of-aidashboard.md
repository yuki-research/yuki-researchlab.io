---
title: Current state of the new AIDashboard
date: 2020-03-05
tags: ["ai", "dashboard", "ui"]
---

The new AI Dashboard project is aimed at benchmarking the performance of all of the rulesets active within Yuki, with planned possibilities of producing automated suggestions, produced with AI, in order to help users and accountants raise the efficiencies of the matching systems within their domains.

The project has received the initial UI treatment, with mocked data.

 <!--more-->

The dashboard now contains the five main windows, and I found it looks better this way (not 2x2 and one long one at the bottom). These are the full view, the 'cramped' resized view & the compact mode that shows only the most necessary information.

![2](/img/03-05/2.png)

Compact mode:

![1](/img/03-05/1.png)

You will also notice I added the standard Yuki date picker at the top, it sort of stands out, but it fits(it will become functional when I start pulling data via ajax).

There are issues im trying to work out with the bottom row, namely that if the "Documents processed" stat is kept, it is named the same for all three bottom row panels (without necessarily portraying the same information).

I have used the MatchingLog table to get information on the counts of transactions matched, and also on which of those were automatic (vs. manual), which is the basis for the efficiency stat. However, I am still looking to explore the 'open' financial matches that should help me add more details to the efficiency statistic, and also find mismatched transactions that would form the base for the accuracy stat.

On Saturday I also started working out how the Data controller for the panels will look like, although I did not get very far because there is no metric tile controller in C#, so we will see how that goes later on. The next stop is trying to find answers about the aforementioned statistic requirements.

Another full-sized image:

![3](/img/03-05/3.png)
