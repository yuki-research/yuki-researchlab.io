---
title: MatchRule clustering demonstration
date: 2020-07-11
tags: ["ai", "dashboard", "ui"]
---

The following is a running model, in Job format, of the hierarchical agglomerative clustering of unmatched BankStatementLines from a domain. The weights are still to be decided fully, I'm still tinkering, but I have already been getting some very valid clusters.

<!--more-->

![1](/img/07-11/1.png)

The 0th cluster, for example, has matching OffsetBankAccount, OffsetName, Descriptions, and Amount delta below 10. Both do not have a MatchRule.

Cluster 3, as an another example, has matching OffsetBankAccount, OffsetName, amount delta below the limit, semi-matching descriptions, and seem to be happening multiple times a month.

These 5 transactions are also not matched by a MatchRule, which means it reduces overall efficiency. A granularity/cluster size of 5 at this (low)dissimilarity level means a very strong contender for a possible new MatchRule.

![2](/img/07-11/2.png)

This is a bit of a clearer view; You will notice a lot of small clusters, and that is because the lower bound on the dissimilarity was set as 0.1 (0 means the BankStatementLines are identical), which is a very high granularity, and is subject for adjustment.

It took 34 seconds to run clustering on 278 data points (unmatched BankStatementLines), and to run recursive dissimilarity+count-bounded search of suitable clusters through the hierarchy. It produced 29 clusters at this granularity level (29 possible new MatchRules).

It will take longer once the MatchRule construction is finished, however. The clustering algorithm just allows for controlling distance, centroid(for clustering quality analysis), etc. calculations for a type of data point, but does not preserve the matching factors (it all gets reduced to a distance). Therefore, I must produce the MatchRules by aggressively looking at the clusters (since they DID match) for matching fields/values. This was the initial algorithm-less model that I started building at the beginning, and I am now modifying it in order to work with these results.
