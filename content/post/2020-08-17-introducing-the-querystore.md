---
title: Introducing the Query Store
date: 2020-08-17
tags: ["databases", "performance"]
---

In 2016 Microsoft introduced the so called Query Store for SQL server. It captures executed queries, execution plans, runtime execution statistics, etc for a given database. Initially intended as a tool to improve SQL Server this query store proves useful in identifying performance problems.

 <!--more-->

![Query Store](/img/querystore.png "Query Store")

Common scenarios for using the Query Store feature are:
-   Determine the number of times a query was executed in a given time window, assisting a DBA in troubleshooting performance resource problems.
-   Identify top n queries (by execution time, memory consumption, etc.) in the past x hours.
-   Audit the history of query plans for a given query.
-   Analyze the resource (CPU, I/O, and Memory) usage patterns for a particular database.
-   Identify top n queries that are waiting on resources.
-   Understand wait nature for a particular query or plan.

## Performance impact
Of course enabling the query store will have its effect on the performance of the database. The design principles of the query store however will ensure that the effect is minimal. When a query gets submitted, the compiled query Execution Plan is written to the Query Store Plan Store and the runtime information of the query is recorded in the Runtime Stats Store. This information is stored initially in-memory. After a specific interval the information inside the in-memory stores is asynchronously written to disk. The interval at which the Query Store flushes data to disk can be configured but, flushing more often comes at a performance cost. By default the flush to disk interval is set to 15 minutes, which results in an average performance overhead of 3-5%.

## Usage scenarios
To aid in improving the database performance it would be great to enable the query store on some important development or pilot domains. The following databases are great candidates for initial exploration of the possibilities of the query store. In the future, for troublesome domains we might want to enable the query store to help in troubleshooting performance issues.

### Central databases
-   Our central database, queried and consulted by all domains.
### Internal databases
-   Large domain with many transactions

## Points of interest
### Regressed Queries:
Pinpoint queries for which execution metrics have recently regressed (for example, changed to worse). Used to correlate observed performance problems in your application with the actual queries that need to be fixed or improved.
### Overall Resource Consumption:
Analyze the total resource consumption for the database for any of the execution metrics. Used to identify resource patterns (daily vs. nightly workloads) and optimize overall consumption for your database.
### Top Resource Consuming Queries:
Identify queries that had the most extreme values for a provided time interval. Used view to focus your attention on the most relevant queries that have the biggest impact to database resource consumption.
### Queries With High Variation:
Analyze queries with high-execution variation as it relates to any of the available dimensions, such as Duration, CPU time, IO, and Memory usage. Used this view to identify queries with widely variant performance that can be affecting user experience across applications.
### Query Wait Statistics:
Analyze wait categories that are most active in a database and which queries contribute most to the selected wait category. Used to analyze wait statistics and identify queries that might be affecting user experience across your applications.

## Permissions
Basically there are three roles when it comes to dealing with the query store:
1.  Administration - turning the store on or off
2.  Viewing results
3.  Forcing execution plans (not required at this point)

For the administration role:
-   Server permission: ALTER ANY DATABASE or Database permission: ALTER

For the view role:
-   Database permission: VIEW DATABASE STATE

## Conclusion
With just a slight decrease in performance, the query store gives you access to a powerful set of options to troubleshoot the performance of a database in sql server. It allows you to identify the pain points, fix them, and tests to see if the performance has improved.

## See also
[https://docs.microsoft.com/en-us/sql/relational-databases/performance/monitoring-performance-by-using-the-query-store?view=sql-server-ver15](https://docs.microsoft.com/en-us/sql/relational-databases/performance/monitoring-performance-by-using-the-query-store?view=sql-server-ver15)

[https://docs.microsoft.com/en-us/sql/relational-databases/performance/query-store-usage-scenarios?view=sql-server-ver15](https://docs.microsoft.com/en-us/sql/relational-databases/performance/query-store-usage-scenarios?view=sql-server-ver15)

[https://www.red-gate.com/simple-talk/sql/database-administration/the-sql-server-2016-query-store-overview-and-architecture/](https://www.red-gate.com/simple-talk/sql/database-administration/the-sql-server-2016-query-store-overview-and-architecture/)

[https://techcommunity.microsoft.com/t5/azure-sql-database/using-query-store-with-least-privileges-instead-of-db-owner-to/ba-p/775177](https://techcommunity.microsoft.com/t5/azure-sql-database/using-query-store-with-least-privileges-instead-of-db-owner-to/ba-p/775177)