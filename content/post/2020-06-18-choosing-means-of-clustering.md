---
title: Choosing means of clustering
date: 2020-06-18
tags: ["ai", "dashboard", "ui"]
---

The following is a information dump, following the process of choosing a clustering algorithm for the MatchRule suggestion engine. Automatic matching is not an option, because finance-related decisions have to be completely transparent due to regulations. The final option ended up being library-supported Hierarchical agglomerative clustering.

Explore the chain of considered options that led to that decision, in this post.

<!--more-->

Notes regarding algorithm choice:

Raw kth nearest neighbor - more suitable for individual transaction handling, cant choose criteria level.
Description is the only flux-component, the other ones are pretty much binary. Meaning that it will have to have an extra layer of normalization, below the universal 'weights' for each of the parameters being matched. Could also just be one weight, although that is less clear.
Hierarchical agglomerative clustering - allows for multiple granularity levels, can use different linking criteria (farthest/closest neighbor, etc). Seems like the best choice.
Needs a library: currently working with https://github.com/pedrodbs/Aglomera.
Support vector classifiers would likely produce better matches but it is best suited for YES/NO cases. Multi-class support vector machine classifiers are really complex since we're dealing with dimensions above 6 or 7, and also the decisions don't really need to be binary.

I also looked into Naive Bayes classifiers, but again, they are more suited for acting on existing data sets in order to then assign the likelihoods of classification, once new data comes in. Since no rule-modification on-the-fly should be happening, and auto-processing of individual transactions is unacceptable, this is not the way forward.

![1](/img/06-18/1.png)

I also looked into spectral clustering and Kernel principal component analysis, more on them below.

This is an example dendogram of the resulting structure after performing hierarchical agglomerative clustering. It groups the data throughout multiple levels of similarity. This way, choosing granularity (how many broad a range of transactions should one rule aim to match) is trivial. Just move the cutoff point up/down, and look at the clusters at that level.

![2](/img/06-18/2.png)

Moreover, this is not directly a suggestion mechanism, it is a matcher of matching criteria in order to produce a rule. The 'suggestion' level is chosen by the granularity (level of the hierarchy).

Suggestions would come on top of this (if one would like to, for example, allow ALL hierarchy levels as choices. So best practices for suggestions do not directly apply.

Rule suggestion based on past-created rules or even rules in combination with how successful each rule is, would produce similar rules, which is not at all what is required here, so typical recommender systems are out, as mentioned before.

This is an another, clearer example of how a very low granularity was chosen to construct the clusters at the bottom (almost at the top of the clustering tree).

![3](/img/06-18/3.png)

Kernel principle component analysis is built on wanting to find boundaries in large sets of multivariate data, but as I said, boundaries don't really matter since either side is likely fine, as long as a reasonable match is made. Moreover, the linkage criteria, would all be difficult to reduce properly, in order to produce an accurate n-dimensional map. Also, the boundaries can be adjusted (granularity), so it does not have to be pin-point accurate.

![4](/img/06-18/4.jpg)
![5](/img/06-18/5.gif)

I also took a look at spectral clustering, which uses basic eigenvector algebra to produce variable-granularity clusters, however I did not take a deeper look since the way to plot out the data is convoluted as mentioned before, and also there is usually some data left unused (the single points in the bottom right graph), which is not ideal if for example a very high granularity is wanted (every transaction has a rule to match). Also, I have not looked into ways to retrieve the clusters (by size, or other metrics).

![6](/img/06-18/6.png)

The final chosen method was Hierarchical agglomerative clustering, with Aglomera. A demo clustered dendogram of unmatched BankStatementLines IDs, linked by their Levenshtein distance (similarity). The distances are all quite high, hence the tallness of the dendogram. There still is clustering at the very top, and I set an example cutoff/granularity level.

![7](/img/06-18/7.png)
