---
title: Producing MatchRule suggestions
date: 2020-07-23
tags: ["ai", "dashboard", "ui"]
---

I've devised a setting combination that holds up fairly well, and produces less, but larger rule sets. These are the properties of the produced rules, as well as counts of transactions matched. However, the description match still misses the mark a little;

<!--more-->

![1](/img/07-23/1.png)

It currently matches sections of descriptions, which are strings of characters that are in the same order somewhere in the two descriptions. This is a lot cleaner than trying to generate nonsense descriptions with just matching words, which usually carries less significance and does not necessarily mean that the descriptions are indeed similar.

However, I seem to be catching some repetetive portions of the descriptions too often. Work continues on ensuring they are not an issue while clustering and also while creating description matches!
