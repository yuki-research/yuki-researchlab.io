---
title: Implementing OpenID Connect
date: 2020-09-14
tags: ["authentication", "oauth"]
draft: true
---

Last month, we added authentication with OpenID Connect to our software.

A few months ago, one of our customers asked us to provide the ability to
integrate with their Azure AD. After some research, we decided that using
OpenID Connect was the most straight-forward and future proof integration.

 <!--more-->

Therefore, we started building it. It was not too hard, even though most
of the available libraries are now optimised for .NET Core and async,
whereas we are still on .NET Framework.

Therefore, sometimes we need to create some wrapper methods, like this:

{{< highlight cs >}}
    public object SomeMethod() {
        return anothermethod().ConfigureAwait(false);
    }
{{</ highlight >}}

Since the prototype is working, we are currently making it production ready.
