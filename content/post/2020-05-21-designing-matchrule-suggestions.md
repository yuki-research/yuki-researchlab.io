---
title: Designing MatchRule suggestions
date: 2020-05-21
tags: ["ai", "dashboard", "ui"]
---

The AI Dashboard has revealed sizeable room for improvement in the accuracy and efficiency of financial Match Rules.

This was the initial thought process, in order to start constructing an engine to help facilitate the improvements.

<!--more-->

I noticed some MatchRules that have been created by users manually matching the payment with the invoice. This could be a good starting point for suggesting rules for the user, but then it also would mean that, in theory, no rule suggestions should ever be necessary given the user has matched all of the open payments to the corresponding invoices. This is just for the MatchingLog for payments with invoices, so there aren't many criteria that could be analysed (fields, names, etc.) to produce concise suggestions that are not completely 'singular'(only match one very specific type of payment), but I will look at the other rule systems to find the best way of integrating suggestions there too.

In any case, I have some good ideas that can apply to suggesting rules for improving both efficiency and accuracy, which have to do with cluster-analyzing all unmatched or non-automatically-handled entries and sorting them out into buckets of pre-defined granularity (a rule which only matches one transaction is not a good rule, so the granularity is one of the main contention points here).
The criteria to be sorted into a bucket also varies by rule system, but in essence, if enough fields match with a high enough certainty between some amount of events/transactions/lines, they are put into a bucket, which is abstracted just enough to accommodate all of the transactions within the bucket, but not anything else.

Then if there are any sizable buckets (e.g. more than 1 event/transactions matched), that bucket's criteria becomes a suggested rule. Even buckets with size one can be taken as a good enough measure to produce a new rule (the granularity issue again), but it must still strive to make more abstract rules, because I assume that is what people want in general. If very targeted rules (that match, say, only one transaction) are okay, then the high granularity may not be an issue.

I have started looking at various methods to produce a map of categorized similarities between rules that constitute the "inefficient" percentage in the charts/panels.

After the initial look through factor and cluster analysis implementations, I expect them to be a little over-engineered, at least for the initial implementation.

The main idea is to produce matches between every "unoptimized" transaction or event, which would produce sets of transactions with some amount of matching fields. Initially I thought that there could be a good medium to choosing for example that if a set has X granularity (fields matched) across some amount of rules and a new rule is matched, but it has potential to be more properly matched, it would still be included with the original set, which would then prevent that transaction/event from being matched again, which may not be the optimal outcome. This scenario could also be flipped, but I don't think a correct merging strategy would be derivable.

So the new idea is to produce mapped matches of granularity that varies from set to set, but is constant within a set for all transactions/events, with duplicates, and then pick the most matching of those sets based on the configured granularity settings. All of the transactions/events would only appear in one "finalized" match set. Then, the set criteria would be transformed into a rule, and thus be ready to suggest.

The alternative to all of this would be dipping into machine learning by constructing a model that would take in both correctly/automatically matched and incorrectly matched but corrected (needs more input info) transactions/events, and would predict the categorization for new transactions/events. This however goes against the transparency principle for making sure all decisions of a categorization system are transparent and explainable.

The alternative would be training with the expected output being the rule that matches every transaction, which should form a model that takes the preferred granularities/general matching characteristics of the existing rules into account in order to output new rules. It could also be fed sets of transactions that are all linked with a rule that is set as the expected output. The training could then be spread over all rule data that is available so that individual new domains do not have to re-train the engine.

Continuing on, I will proceed with formulating the manual suggestion set matching method.
