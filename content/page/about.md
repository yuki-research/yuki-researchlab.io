---
title: About Yuki Research
subtitle: Bringing innovation to the forefront
date: 2020-09-14
comments: false
---

Some blurb about Yuki and the research team, such as:

The Research and Innovation team works on investigating tools and
technologies that may make the Yuki product better in the near future.
